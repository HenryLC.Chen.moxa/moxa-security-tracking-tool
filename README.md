# moxa security tracking tool

## About
Moxa security tracking tool can scan package's vulnerability by input a package list.  


## Usage  

### Config  
Config file named `moxa-security-tracking-tool.conf` is set some information for this tool.  
- security_tracker_path: security tracker source.
- package_list_path: package list source.
- cve_info_api: cve information source. (to get cvss score)
- suite: choose which debian version will use in this tool

#### Example

```
[CONFIG]
security_tracker_path = https://security-tracker.debian.org/tracker/data/json
package_list_path = /home/moxa/DIFB2/DIFB/configs/MIL/Standard/MIL3/armhf/V3.0.0/packages.list
cve_info_api = https://services.nvd.nist.gov/rest/json/cve/1.0/
suite = bullseye
```
### Tool usage
```
usage: moxa-security-tracking-tool.py [-h] -s [-o OUTPUT]

optional arguments:
  -h, --help            show this help message and exit
  -s, --scan            scan vulnerability
  -o OUTPUT, --output OUTPUT
                        output vulnerability report (csv file)
```
use `python3 moxa-security-tracking-tool.py -s` to run.  
use `python3 moxa-security-tracking-tool.py -s -o file_name` to run and get csv report.  

