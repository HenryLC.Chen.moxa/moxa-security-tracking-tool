from re import A
import requests
import json
import argparse
import configparser
import csv
from debian import debian_support

config = configparser.ConfigParser()
config.read('moxa-security-tracking-tool.conf')
security_tracker_path = config['CONFIG']['security_tracker_path']
cve_info_api = config['CONFIG']['cve_info_api']
package_list_path = config['CONFIG']['package_list_path']
suite = config['CONFIG']['suite']

print('=' *170)
print("security_tracker_path: %s" % security_tracker_path)
print("cve_info_api: %s" % cve_info_api)
print("package_list_path: %s" % package_list_path)
print("suite: %s" % suite)
print('=' *170)

vulnerability_result_list = []

def parse_packagelist(file_name):
    package_list = {}
    with open(file_name,'r') as fp:
        for each_line in fp.readlines():
            package_list[each_line.split(', ')[0]] = each_line.split(', ')[1]
    return package_list

def get_security_tracker_data(security_tracker_path):
    r = requests.get(security_tracker_path)
    return r.json()

def print_oneline_result(package_name='package_name', package_version='package_version', cve_key='CVE', cvss_v3_score='cvss3.1', cvss_v2_score='cvss2', fixed_version='fixed_version', description='CVE description'):
    print ("%-20s | %-30s | %-20s | %-8s | %-8s | %-40s | %-50.50s" % (package_name, package_version, cve_key, cvss_v3_score, cvss_v2_score, fixed_version, description))

def collect_result(package_name='package_name', package_version='package_version', cve_key='CVE', cvss_v3_score='cvss3.1', cvss_v2_score='cvss2', fixed_version='fixed_version', description='CVE description'):
    vulnerability_result_list.append({'package_name':package_name,'package_version':package_version,'cve_key':cve_key,'cvss_v3_score':cvss_v3_score,'cvss_v2_score':cvss_v2_score,'fixed_version':fixed_version,'description':description})

def scan_vulnerability(security_tracker_data, suite, package_name, package_version):
    try:
        if security_tracker_data.get(package_name) != None:
            for cve_key,cve_value in security_tracker_data.get(package_name).items():
                for releases_key,releases_value in cve_value.get('releases').items():
                    if releases_key == suite:
                        if releases_value.get('fixed_version') == None:
                            if releases_value.get('status') == 'open':
                                cvss_v3_score, cvss_v2_score = get_cvss(cve_key)
                                collect_result(package_name, package_version, cve_key, cvss_v3_score, cvss_v2_score, "open issue, urgency: %s" % releases_value.get('urgency'), cve_value.get('description'))
                                print_oneline_result(package_name, package_version, cve_key, cvss_v3_score, cvss_v2_score, "open issue, urgency: %s" % releases_value.get('urgency'), cve_value.get('description'))
                            else:
                                cvss_v3_score, cvss_v2_score = get_cvss(cve_key)
                                collect_result(package_name, package_version, cve_key, cvss_v3_score, cvss_v2_score, 'unknown status',cve_value.get('description'))
                                print_oneline_result(package_name, package_version, cve_key, cvss_v3_score, cvss_v2_score, 'unknown status',cve_value.get('description'))
                        else:
                            if debian_support.version_compare(releases_value.get('fixed_version'),package_version) > 0:
                                cvss_v3_score, cvss_v2_score = get_cvss(cve_key)
                                collect_result(package_name, package_version, cve_key, cvss_v3_score, cvss_v2_score, releases_value.get('fixed_version'), cve_value.get('description'))
                                print_oneline_result(package_name, package_version, cve_key, cvss_v3_score, cvss_v2_score, releases_value.get('fixed_version'), cve_value.get('description'))
    except Exception as e:
        print(e)
        print ("ERROR package: %s" % package_name)

def get_cvss(cve_id):
    cvss_v3 = None
    cvss_v2 = None
    path = '%s%s' % (cve_info_api, cve_id)
    try:
        r = requests.get(path)
        r = r.json()
        cvss_impact = r.get('result').get('CVE_Items')[0].get('impact')
        if cvss_impact.get('baseMetricV3') != None:
            cvss_v3 = cvss_impact.get('baseMetricV3').get('cvssV3').get('baseScore')
        if cvss_impact.get('baseMetricV2') != None:
            cvss_v2 = cvss_impact.get('baseMetricV2').get('cvssV2').get('baseScore')
    except Exception as e:
        # print(e)
        cvss_v3 = 'ERROR'
        cvss_v2 = 'ERROR'
    return cvss_v3, cvss_v2

def output_csv(file_name):
    with open(file_name, 'w', newline='') as csvfile:
        fieldnames = ['package_name', 'package_version', 'cve_key', 'cvss_v3_score', 'cvss_v2_score', 'fixed_version', 'description']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for vulnerability_result in vulnerability_result_list:
            writer.writerow(vulnerability_result)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--scan", required=True, help="scan vulnerability", action="store_true")
    parser.add_argument("-o", "--output", help="output vulnerability report (csv file)", type=str)
    args = parser.parse_args()
    if args.scan:
        package_list = parse_packagelist(package_list_path)
        security_tracker_data = get_security_tracker_data(security_tracker_path)
        print_oneline_result()
        print ('=' *170)
        for k in package_list:
            scan_vulnerability(security_tracker_data, suite, k, package_list[k])
        if args.output:
            print ('=' *170)
            print ('output csv file to : %s' % args.output)
            output_csv(args.output)
            print ('output csv file to : %s , success' % args.output)
    else:
        parser.print_help()